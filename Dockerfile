FROM alpine:3.12
RUN apk add --no-cache nginx \
    && mkdir -p /run/nginx && touch /run/nginx/nginx.pid \
    && ln -s /dev/stdout /var/log/nginx/access.log \
    && ln -s /dev/stderr /var/log/nginx/error.log

COPY etc /etc
COPY var /var

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"] 
