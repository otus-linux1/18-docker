.PHONY: build publish pull

build:
	docker build -t nginx-otus:latest .


publish:
	docker tag nginx-otus:latest rbolkhovitin/nginx-otus:latest
	docker push rbolkhovitin/nginx-otus:latest

pull:
	docker pull rbolkhovitin/nginx-otus:latest
